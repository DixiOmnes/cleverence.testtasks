﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cleverence.TestTasks.ReaderWriterServer
{
    public enum EventType { Read = 1, Write = 2 }

    public class TestableServerEvent
    {
        public DateTime Created { get; set; }
        public int Value { get; set; }
        public EventType EventType { get; set; }
    }
}
