﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Cleverence.TestTasks.ReaderWriterServer
{
    public class Server
    {
        private int x = 0;
        private ReaderWriterLockSlim _lock = new ReaderWriterLockSlim();

        public int GetCount()
        {
            _lock.EnterReadLock();

            var tmp = x;

#if CONSISTENCY_TESTING
            Events.Enqueue(new TestableServerEvent() { Created = DateTime.Now, EventType = EventType.Read, Value = x });
#endif
            _lock.ExitReadLock();

            return tmp;
        }

        public void AddToCount(int value)
        {
            _lock.EnterWriteLock();

            Interlocked.Add(ref x, value);

#if CONSISTENCY_TESTING
            Events.Enqueue(new TestableServerEvent() { Created = DateTime.Now, EventType = EventType.Write, Value = x });
#endif

            _lock.ExitWriteLock();
        }

#if CONSISTENCY_TESTING
        public ConcurrentQueue<TestableServerEvent> Events { get; } = new ConcurrentQueue<TestableServerEvent>();
#endif
    }
}
