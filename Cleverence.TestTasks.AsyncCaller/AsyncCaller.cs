﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Cleverence.TestTasks.AsyncCallerLib
{
    public class AsyncCaller
    {
        private readonly EventHandler _eventHandler;

        public AsyncCaller(EventHandler eventHandler)
        {
            _eventHandler = eventHandler ?? throw new ArgumentNullException(nameof(eventHandler));
        }

        public bool Invoke(int timeout, object sender, EventArgs eventArgs)
        {
            var cts = new CancellationTokenSource(timeout);

            var task = Task.Run(() => _eventHandler.Invoke(sender, eventArgs), cts.Token);

            try
            {
                task.Wait(cts.Token);
            }
            catch (OperationCanceledException)
            {
            }

            return task.Status == TaskStatus.RanToCompletion;
        }
    }
}
