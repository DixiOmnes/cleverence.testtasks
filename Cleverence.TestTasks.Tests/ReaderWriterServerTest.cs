using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Cleverence.TestTasks.ReaderWriterServer.Tests
{

    [TestClass]
    public class ReaderWriterServerTest
    {
        /// <summary>
        /// ����� ������� 4 ������-�������� � 2 ������-��������, ������� �������� � Server
        /// ���� � ������� ������� ���������� ������ ��� ���� CONSISTENCY_TESTING, �� ������ ��������� � �������
        /// ����� ��������������� ������������ � ������� �������.
        /// 
        /// ����� ������������� ���������� �������� � �������� ������������� ������� �� ������� ������������ ������ � ������
        /// ���� ����� ������ �������� � ��������� ������ �������� ��������� �� �, �� ����
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestServer()
        {
            //��������� ����� dynamic, ����� ����� ����������, ���� CONSISTENCY_TESTING � ����������� ������� ���������.
            //������ ����� ����������� ���������� �� ����� ���������� �����
            dynamic server = new Server();
            var source = new CancellationTokenSource();

            await Task.Run(() => RunThreads(server, source.Token));

            int setValue = 0;
            bool result = true;

            foreach (var @event in server.Events)
            {
                if (@event.EventType == EventType.Write)
                    setValue = @event.Value;
                else
                    if (@event.EventType == EventType.Read && @event.Value != setValue)
                    {
                        result = false;
                        break;
                    }

            }

            Assert.IsTrue(result);
        }

        private Task RunThreads(Server server, CancellationToken cancellationToken)
        {
            var tasks = new List<Task>();

            int operationCount = 0;
            int maxOperations = 10000000;

            //������ - ��������
            for (int i = 0; i < 4; i++)
            {
                tasks.Add(Task.Run(() =>
                {
                    var rnd = new Random();
                    while (operationCount < maxOperations)
                    {
                        if (cancellationToken.IsCancellationRequested)
                            cancellationToken.ThrowIfCancellationRequested();

                        server.GetCount();

                        Interlocked.Increment(ref operationCount);
                    }
                }, cancellationToken));
            }

            //������-��������
            for (int i = 0; i < 2; i++)
            {
                tasks.Add(Task.Run(() =>
                {
                    var rnd = new Random();
                    while (operationCount < maxOperations)
                    {
                        if (cancellationToken.IsCancellationRequested)
                            cancellationToken.ThrowIfCancellationRequested();

                        var x = rnd.Next(-50, 50);

                        server.AddToCount(x);

                        Interlocked.Increment(ref operationCount);

                        Thread.Sleep(rnd.Next(1, 50));
                    }
                }, cancellationToken));
            }

            try
            {
                Task.WaitAll(tasks.ToArray());
            }
            catch (AggregateException ex)
            {
                ex.Handle(ex => ex is OperationCanceledException);
            }

            return Task.CompletedTask;
        }
    }
}
