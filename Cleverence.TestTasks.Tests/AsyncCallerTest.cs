﻿using Cleverence.TestTasks.AsyncCallerLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cleverence.TestTasks.ReaderWriterServer.Tests
{
    [TestClass]
    public class AsyncCallerTest
    {
        [TestMethod]
        public void TestAsyncCaller()
        {
            Assert.IsFalse(new AsyncCaller(new EventHandler(OutOfTime)).Invoke(3000, null, EventArgs.Empty));
            Assert.IsTrue(new AsyncCaller(new EventHandler(RantoCompletion)).Invoke(5000, null, EventArgs.Empty));

            try
            {
                new AsyncCaller(new EventHandler(Exception)).Invoke(5000, null, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                Assert.AreEqual(ex.InnerException?.Message, "smth went wrong");
                return;
            }

            Assert.Fail();
        }

        private void OutOfTime(object sender, EventArgs e)
        {
            Thread.Sleep(10000);
        }

        private void RantoCompletion(object sender, EventArgs e)
        {
            Thread.Sleep(2000);
        }

        private void Exception(object sender, EventArgs e)
        {
            throw new Exception("smth went wrong");
        }
    }
}
